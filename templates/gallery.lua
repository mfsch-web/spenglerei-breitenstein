str = pandoc.utils.stringify

function add_category_list(m)
  local current = str(m.category)
  print("category:", current)
  for i,v in ipairs(m.objects) do
      local fn = "content/objekte/"..str(v.id)..".md"
      local thumb = 1
      if v.thumbnail ~= nil then
          thumb = tonumber(str(v.thumbnail))
      end
      local img = pandoc.read(io.lines(fn, "*a")()).meta.images[thumb]
      v.image = img.id
      if i == 13 then
          v.gap = pandoc.MetaBool(true)
      end
  end
end
-- $if(objects)$<ul id=objects>
-- $for(objects)$
--   <li><a href="/referenzobjekte/$category$/$it.id$"><img src="/img/gallery/$it.image$-small.jpg" /></a></li>
-- $endfor$
-- </ul>$endif$

function expand_paths(m)
  for i,v in ipairs(m.categories) do
    local path = "/referenzobjekte/"..str(v.name):lower()
    v.path = pandoc.MetaString(path)
    for i,v in ipairs(v.objects) do
        v.path = pandoc.MetaString(path.."/"..str(v.id))
    end
  end
end

function add_object_details(m)

  -- parse category & object id
  local path = str(m.object)
  local sep = path:find("/")
  local cat = path:sub(1,sep-1)
  local obj = path:sub(sep+1)

  -- check if switcher should be shown
  if #m.images > 1 then
    m["show-switcher"] = pandoc.MetaBool(true)
  end

  -- add info from category data
  local all_obj = pandoc.read(io.lines("content/kategorien/"..cat..".md", "*a")()).meta.objects
  for i,v in ipairs(all_obj) do
    if str(v.id) == obj then

      -- mark image that is shown in the overview
      local featured = 1
      if v.thumbnail ~= nil then
        featured = tonumber(str(v.thumbnail))
      end
      m.images[featured].featured = pandoc.MetaBool(true)

      -- add navigation to previous/next object
      local nav = pandoc.MetaMap({ up = pandoc.MetaString(cat) })
      if i > 1 then
        nav.prev = pandoc.MetaString(cat.."/"..str(all_obj[i-1].id))
      end
      if i < #all_obj then
        nav.next = pandoc.MetaString(cat.."/"..str(all_obj[i+1].id))
      end
      m.nav = nav

      break
    end
  end
end

function Meta(m)
  if m["expand-paths"] ~= nil then
    expand_paths(m) -- build list of paths for dependencies
  end
  if m["category"] ~= nil then
    add_category_list(m) -- build list of objects for category overview
  end
  if m["object"] ~= nil then
    add_object_details(m) -- add data for object detail page
  end
  return m
end
