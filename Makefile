# settings
SITE = public
TOOLS = tools
PANDOC_VERSION = 2.11.2
SASS_VERSION = 1.29.0

.DEFAULT_GOAL := site
.PHONY: site clean

pandoc = $(TOOLS)/pandoc-$(PANDOC_VERSION)/bin/pandoc
pandoc_cmd = $(pandoc) --from=markdown-auto_identifiers-implicit_figures --wrap=preserve
sass = $(TOOLS)/dart-sass/sass

content := $(patsubst content/%.md,/%,$(wildcard content/*.md))
static := $(patsubst static/%,/%,$(wildcard static/*) $(wildcard static/*/*))
static := $(filter-out $(patsubst %/,%,$(dir $(static))),$(static))
$(info content: $(content))
$(info static: $(static))

# object database
images := $(patsubst images/%,%,$(wildcard images/*.jpg))
categories := $(patsubst content/kategorien/%.md,/referenzobjekte/%,$(wildcard content/kategorien/*.md))
objects := $(shell for f in content/kategorien/*.md; do cat=$$(basename -s .md $$f); grep ' id: ' $$f | sed "s/.*id: \(.*\)/\/referenzobjekte\/$$cat\/\1/"; done)
$(info categories: $(categories))
$(info objects: $(objects))

paths = $(addsuffix .html,$(content) $(categories) $(objects)) \
    /css/main.css \
    $(patsubst %.jpg,/img/gallery/%.jpg,$(images)) \
    $(patsubst %.jpg,/img/gallery/%-small.jpg,$(images)) \
    $(static)

site: $(addprefix $(SITE),$(paths))

clean:
	rm -rf $(SITE) $(TOOLS)

$(pandoc):
	@mkdir -p $(TOOLS)
	wget -O $(TOOLS)/pandoc.tar.gz https://github.com/jgm/pandoc/releases/download/$(PANDOC_VERSION)/pandoc-$(PANDOC_VERSION)-linux-amd64.tar.gz
	tar -xzf $(TOOLS)/pandoc.tar.gz -C $(TOOLS)

$(sass):
	@mkdir -p $(TOOLS)
	wget -O $(TOOLS)/dart-sass.tar.gz https://github.com/sass/dart-sass/releases/download/$(SASS_VERSION)/dart-sass-$(SASS_VERSION)-linux-x64.tar.gz
	tar -xzf $(TOOLS)/dart-sass.tar.gz -C $(TOOLS)

$(addprefix $(SITE),$(static)): $(SITE)/%: static/%
	@mkdir -p $(@D)
	cp $< $@

$(SITE)/css/%.css: styles/%.scss $(wildcard styles/_*.scss) | $(sass)
	@mkdir -p $(@D)
	$(sass) $< $@

$(SITE)/img/gallery/%.jpg: images/%.jpg
	@mkdir -p $(@D)
	cp $< $@

$(SITE)/img/gallery/%-small.jpg: images/%.jpg
	mkdir -p $(@D)
	convert $< -scale 138x87^ -gravity center -extent 138x87 +profile "*" -sharpen 0x1.0 $@

$(SITE)/%.html: content/%.md $(wildcard templates/*.html) | $(pandoc)
	@mkdir -p $(@D)
	$(pandoc_cmd) --template=templates/default.html \
	    --variable=page-$(shell echo $* | cut -d'/' -f1) \
	    --output=$@ $<

$(SITE)/team-werkstatt.html: content/team-werkstatt.md $(wildcard templates/*.html) | $(pandoc)
	@mkdir -p $(@D)
	$(pandoc_cmd) --template=templates/gallery.html \
	    --variable=page-team-werkstatt \
	    --output=$@ $<

# object category overview pages
$(patsubst %,$(SITE)%.html,$(categories)): $(SITE)/referenzobjekte/%.html: content/kategorien/%.md $(wildcard templates/*) templates/gallery.lua | $(pandoc)
	@mkdir -p $(@D)
	$(pandoc_cmd) --template=templates/gallery.html \
	    --metadata=category:$* \
	    --variable=category-$* \
	    --variable=page-referenzobjekte \
	    --lua-filter=templates/gallery.lua \
	    --output=$@ $<

.SECONDEXPANSION:

# single object detail pages
$(patsubst %,$(SITE)%.html,$(objects)): $(SITE)/referenzobjekte/%.html: content/objekte/$$(shell echo % | cut -d'/' -f2).md $(wildcard templates/*) templates/gallery.lua | $(pandoc)
	@mkdir -p $(@D)
	$(pandoc_cmd) --template=templates/gallery.html \
	    --metadata=object:$* \
	    --variable=category-$(shell echo $* | cut -d'/' -f1) \
	    --variable=page-referenzobjekte \
	    --lua-filter=templates/gallery.lua \
	    --output=$@ $<
