// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());


// load interactive functions when DOM is ready
$(document).ready(function() {

  // load gallery functionality
  $("#thumbnails a").click(function(e) {
    e.preventDefault();
    var image = $(this).attr("href");
    $('#img-large').fadeOut('slow', function() {
      $(this).attr('src',image).bind('onreadystatechange load', function() {
        if (this.complete) $(this).fadeIn('slow');
      });
    });
  });

  // toggle credits
  $('footer h2').click(function(event) {
    $('footer ul').slideToggle();
  });

  // toggle submenu
  //$('nav>ul a').first().click(function(event) {
  //  event.preventDefault();
  //  $('nav ul ul').slideToggle();
  //});

});
