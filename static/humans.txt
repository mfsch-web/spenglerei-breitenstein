# TEAM

    Wortgewandt   -- Text   -- www.wortgewandt.ch
    Edith Spettig -- Design -- www.vistapoint.ch
    Manuel Schmid -- Code   -- dergestalt.ch

# TECHNOLOGY COLOPHON

    HTML5, CSS3
    Normalize.css, jQuery, Modernizr
    Perch CMS
