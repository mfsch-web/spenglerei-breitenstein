---
title: Stil und Ästhetik
description: "Wir achten bei Um- und Ausbauten darauf, den Charakter des Gebäudes zu wahren. Bei Neubauten sorgen wir dafür, dass das Dach stilistisch zur Umgebung passt."
---

Klar: Bei Spengler- und Bedachungsarbeiten ist das technische Können ausschlaggebend. Wir denken aber auch an die Ästhetik. So achten wir bei Um- und Ausbauten darauf, den Charakter des Gebäudes zu wahren. Bei Neubauten sorgen wir dafür, dass das Dach stilistisch zur Umgebung passt.

Bei der Isolation von Dächern wählen wir sorgfältig die passenden Materialien aus. Nach Möglichkeit arbeiten wir mit organischen, ökologischen Produkten wie Zelluloseflocken, Zellulose- oder flexiblen Holz­faserplatten. Eine weitere Option sind anorganische Isolationsstoffe wie Styropor-Platten, Glas- oder Steinwolle.
