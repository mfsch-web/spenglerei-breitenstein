---
title: "Marc Breitenstein | Spenglerei & Bedachung"
description: "Unsere Firma bietet Ihnen sorgfältige, termin- und fachgerechte Dienstleistungen zu einem vernünftigen Preis in der Region Basel an."
emergency-number: "+41 076 336 68 84"
---

Herzlich willkommen über den Dächern Basels! Bei uns sind Sie in den besten Händen, denn wir sind die Spezialisten für Spenglerarbeiten an Steil-, Flach- und Schieferdächern.

Wir planen, bauen, reparieren, isolieren und begrünen. Wir bauen Dachfenster und Solaranlagen ein. Und all dies fachgerecht, sorgfältig, termintreu und zu einem vernünftigen Preis.

Bevor wir loslegen, machen wir uns ein Bild des Gebäudes und besprechen mit Ihnen die beste Lösung. Lassen Sie sich von uns beraten – wir stehen Ihnen gerne mit unserem Know-how zur Seite!
