---
title: Team und Werkstatt
description: "Einige Eindrücke des Teams und der Werkstatt von Marc Breitenstein, Spenglerei &amp; Bedachungen."
images:
  - id: 01
    featured: true
  - id: 02
  - id: 03
  - id: 04
  - id: 05
  - id: 06
show-switcher: true
---
