---
title: Kontakt
description: "Sie finden uns in unserer Werkstatt in Aesch oder erreichen uns per Telefon (076 336 68 84) oder E-Mail."
contact:
  description: Spenglerei • Bedachungen
  owner: Marc Breitenstein
  street: Industriestrasse 45
  zip: 4147
  town: Aesch
  phone: +41 076 336 68 84
  map-url: https://map.search.ch/4147-aesch/industriestr.45
---
