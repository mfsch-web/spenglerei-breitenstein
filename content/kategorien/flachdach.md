---
title: Flachdach
description: "Referenzobjekte mit Flachdach-Arbeiten von Marc Breitenstein, Spenglerei &amp; Bedachungen"
objects:
  - id: 25
  - id: 20
    thumbnail: 2
  - id: 11
  - id: 23
  - id: 24
  - id: 26
  - id: 29
  - id: 31
  - id: 43
  - id: 36
  - id: 41
---
